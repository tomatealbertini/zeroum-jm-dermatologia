//
//  PacienteManager.h
//  jmdermatologia
//
//  Created by Zeroum on 17/05/13.
//
//

#import <Foundation/Foundation.h>
#import "Paciente.h"
#import <sqlite3.h>

@interface PacienteManager : NSObject {

    NSMutableString *retorno;
    
}

+ (PacienteManager *)sharedManager;
- (NSString *)getDataBasePath;
- (void)gravarPaciente:(Paciente *)p;
- (NSDictionary*)pacientesCadastrados;
- (NSArray*)datasCadastros;
- (void)excluirPaciente:(NSInteger)idd;

@end
