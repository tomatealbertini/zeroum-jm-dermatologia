//
//  Paciente.h
//  jmdermatologia
//
//  Created by Zeroum on 17/05/13.
//
//

#import <Foundation/Foundation.h>

@interface Paciente : NSObject {
    
}

@property (nonatomic, assign) NSInteger idd;
@property (nonatomic, retain) NSString *nome;
@property (nonatomic, retain) NSString *dataNascimento;
@property (nonatomic, retain) NSString *profissao;
@property (nonatomic, retain) NSString *cpf;
@property (nonatomic, retain) NSString *rg;
@property (nonatomic, retain) NSString *endereco;
@property (nonatomic, retain) NSString *cep;
@property (nonatomic, retain) NSString *cidade;
@property (nonatomic, retain) NSString *bairro;
@property (nonatomic, retain) NSString *telefone;
@property (nonatomic, retain) NSString *celular;
@property (nonatomic, retain) NSString *email;
@property (nonatomic, retain) NSString *indicacao;
@property (nonatomic, assign) BOOL contatoSms;
@property (nonatomic, assign) BOOL contatoEmail;
@property (nonatomic, assign) BOOL contatoTelefone;
@property (nonatomic, retain) NSString *dataCadastro;

@end
