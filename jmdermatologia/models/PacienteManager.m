//
//  PacienteManager.m
//  jmdermatologia
//
//  Created by Zeroum on 17/05/13.
//
//

#import "PacienteManager.h"
#import "FMDatabase.h"

@interface PacienteManager()

@end



@implementation PacienteManager


static PacienteManager *_sharedManager = nil;

+ (PacienteManager *)sharedManager
{
	@synchronized([PacienteManager class]) {
		if (!_sharedManager)
			_sharedManager = [[self alloc] init];
		
		return _sharedManager;
	}
	return nil;
}

+ (id)alloc {
	@synchronized([PacienteManager class])
    {
		NSAssert(_sharedManager == nil, @"Attempted to allocate a second instance of a singleton.");
		_sharedManager = [super alloc];
		return _sharedManager;
	}
	
	return nil;
}

- (id)init {
	if (self = [super init]) {
       
	}
	
	return self;
}

- (void) dealloc {
    
    [super dealloc];
}




















- (NSString *)getDataBasePath
{
	NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDir = [documentPaths objectAtIndex:0];
	return [documentsDir stringByAppendingPathComponent:DB_NOME];
}

- (void)gravarPaciente :(Paciente *)p
{
    FMDatabase *db = [FMDatabase databaseWithPath: [self getDataBasePath]];
	
	if (![db open])
    {
		NSLog(@"Erro ao abrir o banco de dados.");
	}
    
    
    UIDatePicker *datepicker = [[UIDatePicker alloc] init];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    unsigned unitFlags = NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit;
    
    NSDateComponents *components = [calendar components:unitFlags fromDate:datepicker.date];
    NSDate *myNewDate = [calendar dateFromComponents:components];
    
    NSString *dataCadastro = [[NSString stringWithFormat:@"%@",myNewDate] substringToIndex:10];
    
    NSArray *arrayData = [dataCadastro componentsSeparatedByString:@"-"];
    
    NSString *insertSQL = [NSString stringWithFormat:@"insert into paciente (nome,dataNascimento,profissao,cpf,rg,endereco,cep,cidade,bairro,telefone,celular,email,indicacao,contatoSms,contatoEmail,contatoTelefone,dataCadastro) values ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@',%d,%d,%d,'%@')",
                           p.nome,
                           p.dataNascimento,
                           p.profissao,
                           p.cpf,
                           p.rg,
                           p.endereco,
                           p.cep,
                           p.cidade,
                           p.bairro,
                           p.telefone,
                           p.celular,
                           p.email,
                           p.indicacao,
                           p.contatoSms,
                           p.contatoEmail,
                           p.contatoTelefone,
                           [NSString stringWithFormat:@"%@/%@/%@",[arrayData objectAtIndex:2],[arrayData objectAtIndex:1],[arrayData objectAtIndex:0]]];
    
    
    NSLog(@"%@",insertSQL);
    
    [db executeUpdate:insertSQL] ? NSLog(@"INSERT -  OK") : NSLog(@"INSERT -  ERRO");

    NSLog(@"%@",[db lastErrorMessage]);
    
    [db close];
    
    [datepicker release];
    [calendar release];
}

- (NSArray*)datasCadastros {
    
    FMDatabase *db = [FMDatabase databaseWithPath: [self getDataBasePath]];
	NSMutableArray *arrDatas = [[NSMutableArray alloc] init];
	
	if (![db open])
    {
		NSLog(@"Erro ao abrir o banco de dados.");
	}
    
	FMResultSet *rs;
	NSString *sql = [[NSString alloc] initWithFormat:@"select id,dataCadastro from paciente group by dataCadastro order by id"];
    
	rs = [db executeQuery:sql];
	[sql release];
    
    while ([rs next])
    {
        NSLog(@"%@",[rs stringForColumn:@"id"]);    
        NSLog(@"%@",[rs stringForColumn:@"dataCadastro"]);
        
		[arrDatas addObject:[rs stringForColumn:@"dataCadastro"]];
	}
    
	NSLog(@"%@",[db lastErrorMessage]);
	
    [rs close];
	[db close];
	
    NSArray *arrRetorno = [NSArray arrayWithArray: arrDatas];
	[arrDatas release];
	
	return arrRetorno;
}

- (NSDictionary*) pacientesCadastrados
{
    FMDatabase *db = [FMDatabase databaseWithPath: [self getDataBasePath]];
	
	if (![db open])
    {
		NSLog(@"Erro ao abrir o banco de dados.");
	}
    
	FMResultSet *rs = nil;
	NSMutableString *sql = [[NSMutableString alloc] init];
    
    NSMutableDictionary *dictPacientes = [[[NSMutableDictionary alloc] init] autorelease];
    
    for (NSString *data in [self datasCadastros]) {
        
        [sql setString:[NSString stringWithFormat:@"select id,nome,dataNascimento,profissao,cpf,rg,endereco,cep,cidade,bairro,telefone,celular,email,indicacao,contatoSms,contatoEmail,contatoTelefone,dataCadastro from paciente where dataCadastro = '%@'",data]];
        
        rs = [db executeQuery:sql];
        
        NSMutableArray *arrPacientes = [[NSMutableArray alloc] init];
        while ([rs next])
        { 
            Paciente *p = [[Paciente alloc] init];
            
            [p setNome:[rs stringForColumn:@"nome"]];
            [p setIdd:[rs intForColumn:@"id"]];
            [p setDataNascimento:[rs stringForColumn:@"dataNascimento"]];
            [p setProfissao:[rs stringForColumn:@"profissao"]];
            [p setCpf:[rs stringForColumn:@"cpf"]];
            [p setRg:[rs stringForColumn:@"rg"]];
            [p setEndereco:[rs stringForColumn:@"endereco"]];
            [p setCep:[rs stringForColumn:@"cep"]];
            [p setCidade:[rs stringForColumn:@"cidade"]];
            [p setBairro:[rs stringForColumn:@"bairro"]];
            [p setCelular:[rs stringForColumn:@"celular"]];
            [p setTelefone:[rs stringForColumn:@"telefone"]];
            [p setEmail:[rs stringForColumn:@"email"]];
            [p setIndicacao:[rs stringForColumn:@"indicacao"]];
            [p setContatoSms:[rs intForColumn:@"contatoSms"]];
            [p setContatoEmail:[rs intForColumn:@"contatoEmail"]];
            [p setContatoTelefone:[rs intForColumn:@"contatoTelefone"]];
            [p setDataCadastro:[rs stringForColumn:@"dataCadastro"]];

            [arrPacientes addObject:p];
            [p release];
        }
        
        [dictPacientes setObject:arrPacientes forKey:data];
        [arrPacientes release];
    }
    
    
    [sql release];
    
	NSLog(@"%@",[db lastErrorMessage]);
	
    [rs close];
	[db close];
	
	return dictPacientes;
}

- (void)excluirPaciente:(NSInteger)idd
{
    FMDatabase *db = [FMDatabase databaseWithPath: [self getDataBasePath]];
	
	if (![db open])
    {
		NSLog(@"Erro ao abrir o banco de dados.");
	}
    
    NSString *insertSQL = [NSString stringWithFormat:@"delete from paciente where id = %d",idd];
    
    [db executeUpdate:insertSQL] ? NSLog(@"DELETE -  OK") : NSLog(@"DELETE -  ERRO");
    
    NSLog(@"%@",[db lastErrorMessage]);
    
    [db close];
}







@end
