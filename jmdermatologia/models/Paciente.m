//
//  Paciente.m
//  jmdermatologia
//
//  Created by Zeroum on 17/05/13.
//
//

#import "Paciente.h"

@implementation Paciente

@synthesize idd;
@synthesize nome;
@synthesize dataNascimento;
@synthesize profissao;
@synthesize cpf;
@synthesize rg;
@synthesize cep;
@synthesize endereco;
@synthesize cidade;
@synthesize bairro;
@synthesize telefone;
@synthesize celular;
@synthesize email;
@synthesize indicacao;
@synthesize contatoSms;
@synthesize contatoEmail;
@synthesize contatoTelefone;
@synthesize dataCadastro;



@end
