//
//  AppDelegate.h
//  jmdermatologia
//
//  Created by ZeroUm on 14/05/13.
//
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
    
}

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@property (nonatomic, retain) NSString *databasePath;
@property (nonatomic, retain) NSString *databaseName;
@end
