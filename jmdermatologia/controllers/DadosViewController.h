//
//  DadosViewController.h
//  jmdermatologia
//
//  Created by Zeroum on 20/05/13.
//
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MessageUI.h>

@interface DadosViewController : UIViewController <MFMailComposeViewControllerDelegate,UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate> {
    NSDictionary *pacientes;
    NSArray *datasCadastradas;
    BOOL editando;
    BOOL selecionando;
    NSMutableArray *selecionados;
}

@property (nonatomic, assign) IBOutlet UITableViewCell *customCell;
@property (nonatomic, assign) IBOutlet UITableView *tbView;
@property (nonatomic, assign) IBOutlet UIButton *btnOpcoes;
@property (nonatomic, assign) IBOutlet UIButton *btnCancelar;
@property (nonatomic, assign) IBOutlet UIButton *btnEnviar;
@property (nonatomic, assign) IBOutlet UIButton *btnSair;

- (IBAction)fechar:(id)sender;
- (IBAction)showActionSheet:(id)sender;
- (IBAction)enviarEmail:(id)sender;

@end
