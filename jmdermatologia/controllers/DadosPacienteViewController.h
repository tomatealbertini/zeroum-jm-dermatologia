//
//  DadosPacienteViewController.h
//  jmdermatologia
//
//  Created by Zeroum on 21/05/13.
//
//

#import <UIKit/UIKit.h>
#import "Paciente.h"
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MessageUI.h>

@interface DadosPacienteViewController : UIViewController <MFMailComposeViewControllerDelegate,UITableViewDataSource,UITableViewDelegate> {
    NSMutableArray *dadosPaciente;
    NSMutableArray *campos;
    NSMutableString *formadecontato;
    
}

@property (nonatomic, assign) IBOutlet UILabel *nome;
@property (nonatomic, assign) IBOutlet UILabel *dataNascimento;
@property (nonatomic, assign) IBOutlet UILabel *profissao;
@property (nonatomic, assign) IBOutlet UILabel *cpf;
@property (nonatomic, assign) IBOutlet UILabel *rg;
@property (nonatomic, assign) IBOutlet UILabel *endereco;
@property (nonatomic, assign) IBOutlet UILabel *bairro;
@property (nonatomic, assign) IBOutlet UILabel *cidade;
@property (nonatomic, assign) IBOutlet UILabel *cep;
@property (nonatomic, assign) IBOutlet UILabel *celular;
@property (nonatomic, assign) IBOutlet UILabel *telefone;
@property (nonatomic, assign) IBOutlet UILabel *email;
@property (nonatomic, assign) IBOutlet UILabel *indicacao;
@property (nonatomic, assign) IBOutlet UILabel *formaContato;
@property (nonatomic, retain) Paciente *paciente;
@property (nonatomic, assign) IBOutlet UITableViewCell *customCell;


@end
