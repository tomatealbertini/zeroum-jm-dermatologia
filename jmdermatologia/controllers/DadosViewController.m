//
//  DadosViewController.m
//  jmdermatologia
//
//  Created by Zeroum on 20/05/13.
//
//

#import "DadosViewController.h"
#import "PacienteManager.h"
#import "DadosPacienteViewController.h"
#import "Helper.h"

@interface DadosViewController ()

@end

@implementation DadosViewController
@synthesize customCell;
@synthesize tbView;
@synthesize btnOpcoes;
@synthesize btnCancelar;
@synthesize btnEnviar;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    selecionados = [[NSMutableArray alloc] init];
    pacientes = [[NSDictionary alloc] initWithDictionary:[[PacienteManager sharedManager] pacientesCadastrados]];
    datasCadastradas = [[NSArray alloc] initWithArray:[[PacienteManager sharedManager] datasCadastros]];
    
    [btnCancelar addTarget:self action:@selector(habilitarSelecao) forControlEvents:UIControlEventTouchUpInside];
    [btnEnviar addTarget:self action:@selector(enviarEmail:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [datasCadastradas count];
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [datasCadastradas objectAtIndex:section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    NSString *data = [datasCadastradas objectAtIndex:section];
    NSArray *pacientesData = [NSArray arrayWithArray:[pacientes objectForKey:data]];
    return [pacientesData count];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DadosViewCell"];
    
    if (cell == nil)
    {
        [[NSBundle mainBundle] loadNibNamed:@"DadosViewCell" owner:self options:nil];
        
        cell = self.customCell;
        self.customCell  = nil;
    }
    
    NSString *data = [datasCadastradas objectAtIndex:indexPath.section];
    
    NSArray *pacientesData = [NSArray arrayWithArray:[pacientes objectForKey:data]];
    
    Paciente *p = [pacientesData objectAtIndex:indexPath.row];
    
    UILabel *nome = (UILabel*)[cell viewWithTag:2];
    
    nome.text = p.nome;
    
    if (selecionando == YES)
    {
        if ([selecionados containsObject:indexPath]) {
            [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
        }
        else
        {
            [cell setAccessoryType:UITableViewCellAccessoryNone];
        }
    }
    else
    {
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    }
    
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if (selecionando == YES) {
        if ([selecionados containsObject:indexPath])
        {
            [selecionados removeObject:indexPath];
            [cell setAccessoryType:UITableViewCellAccessoryNone];
        }
        else
        {
            [selecionados addObject:indexPath];
            [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
        }
    }
    else
    {
        NSString *data = [datasCadastradas objectAtIndex:indexPath.section];
        NSArray *pacientesData = [NSArray arrayWithArray:[pacientes objectForKey:data]];
        Paciente *p = [pacientesData objectAtIndex:indexPath.row];
        DadosPacienteViewController *dadosPacienteController = [[DadosPacienteViewController alloc] initWithNibName:@"DadosPacienteView" bundle:nil];
        [dadosPacienteController setPaciente:p];
        [self.navigationController pushViewController:dadosPacienteController animated:YES];
        [dadosPacienteController release];
    }
    
    
}

- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return [pacientes count] > 0;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        NSString *data = [datasCadastradas objectAtIndex:indexPath.section];
        NSArray *pacientesData = [NSArray arrayWithArray:[pacientes objectForKey:data]];
        Paciente *p = [pacientesData objectAtIndex:indexPath.row];
        [[PacienteManager sharedManager] excluirPaciente:p.idd];
        [self reloadData];
    }
}

#pragma mark - Action Sheet

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) //apagar pacientes
    {
        [self habilitarExclusao];
    }
    else if (buttonIndex == 2) //marcar varios para envio de email
    {
        [self habilitarSelecao];
    }
}

#pragma mark - Private


- (void) reloadData
{
    [self viewDidLoad];
    [self.tbView reloadData];
}

- (void)habilitarExclusao
{
    editando = !editando;
    [self.tbView setEditing:editando animated:YES];
    [self.btnOpcoes setSelected:editando];
}

- (void)habilitarSelecao
{
    
    selecionando = !selecionando;
    if (selecionando == YES)
    {
        [self.tbView reloadData];
        [self.btnOpcoes setHidden:YES];
        [self.btnEnviar setHidden:NO];
        [self.btnCancelar setHidden:NO];
        [self.btnSair setHidden:YES];
    }
    else
    {
        [self.btnOpcoes setHidden:NO];
        [self.btnEnviar setHidden:YES];
        [self.btnCancelar setHidden:YES];
        [self.btnSair setHidden:NO];
    }
    [self.tbView reloadData];
}

- (NSString*)montaFormaDeContato:(Paciente*)paciente {
    
    NSMutableString *formadecontato = [[NSMutableString alloc] initWithCapacity:20];
    
    NSInteger quantidadeFormaContato = 0;
    
    if (paciente.contatoSms == YES)
    {
        quantidadeFormaContato++;
        [formadecontato appendString:@"SMS"];
    }
    
    if (paciente.contatoEmail == YES)
    {
        if (quantidadeFormaContato > 0)
        {
            [formadecontato appendString:@" / E-mail"];
        }
        else
        {
            [formadecontato appendString:@"E-mail"];
        }
        quantidadeFormaContato++;
    }
    
    if (paciente.contatoTelefone == YES)
    {
        if (quantidadeFormaContato > 0)
        {
            [formadecontato appendString:@" / Telefone"];
        }
        else
        {
            [formadecontato appendString:@"Telefone"];
        }
    }
    
    NSString *retorno = [NSString stringWithString:formadecontato];
    
    [formadecontato release];
    
    return retorno;
}

#pragma mark - Email

- (NSString *)montarEmail {
    
    NSMutableString *corpoEmail = [[[NSMutableString alloc] init] autorelease];

    NSString *arquivoHeader = @"template-header";
    NSString *arquivoBody = @"template-body";

    NSString *filePath = [[NSBundle mainBundle] pathForResource:arquivoHeader ofType:@"html"];
	NSData *htmlData   = [NSData dataWithContentsOfFile:filePath];
	NSMutableString *templateHeader = [[NSMutableString alloc] initWithData:htmlData encoding:NSUTF8StringEncoding];
    
    [corpoEmail appendString:templateHeader];
    
    filePath = [[NSBundle mainBundle] pathForResource:arquivoBody ofType:@"html"];
    htmlData = [NSData dataWithContentsOfFile:filePath];
    NSString *templateBody = [[NSString alloc] initWithData:htmlData encoding:NSUTF8StringEncoding];
    
    for (NSIndexPath *indexPath in selecionados)
    {
        
        NSString *data = [datasCadastradas objectAtIndex:indexPath.section];
        NSArray *pacientesData = [NSArray arrayWithArray:[pacientes objectForKey:data]];
        Paciente *p = [pacientesData objectAtIndex:indexPath.row];
        
        [corpoEmail appendString:[NSString  stringWithFormat:templateBody,
                          p.nome,
                          p.dataCadastro,
                          p.nome,
                          p.dataNascimento,
                          p.profissao,
                          p.cpf,
                          p.rg,
                          p.endereco,
                          p.bairro,
                          p.cidade,
                          p.cep,
                          p.celular,
                          p.telefone,
                          p.email,
                          p.indicacao,
                          [self montaFormaDeContato:p]]];
    }
    
    filePath = [[NSBundle mainBundle] pathForResource:@"template-footer" ofType:@"html"];
    htmlData = [NSData dataWithContentsOfFile:filePath];
    NSString *templateFooter = [[NSString alloc] initWithData:htmlData encoding:NSUTF8StringEncoding];
    
    [corpoEmail appendString:templateFooter];
    
    [templateHeader release];
    [templateFooter release];
    [templateBody release];
    
    return corpoEmail;
}
- (IBAction)enviarEmail:(id)sender
{
    if ([selecionados count] > 0)
    {
        MFMailComposeViewController* mailController = [[MFMailComposeViewController alloc] init];
        
        if ([MFMailComposeViewController canSendMail])
        {
            mailController.mailComposeDelegate = self;
            [mailController setToRecipients:[NSArray arrayWithObject:@"contato@jmdermatologia.com.br"]];
            [mailController setSubject:@"Dados de pacientes selecionados"];
            [mailController setMessageBody:[self montarEmail] isHTML:YES];
            
            [self presentViewController:mailController animated:YES completion:^{
                ;
            }];
        }
        
        [mailController release];
    }
    else
    {
        [Helper alertViewMessage:@"Selecione os pacientes na listagem" andTitle:@"Atenção"];
    }
}


- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error
{
	if (result == MFMailComposeResultSent)
    {
		NSLog(@"Email enviado!");
        [Helper alertViewMessage:@"Email Enviado com sucesso!" andTitle:@"Atenção"];
    }
	
	[self dismissViewControllerAnimated:YES completion:^{
        ;
    }];
}

#pragma mark - IBActions

- (IBAction)showActionSheet:(id)sender
{
    if ([btnOpcoes isSelected] == YES)
    {
        if (editando == YES)
        {
            [self habilitarExclusao];
        }
    }
    else
    {
        NSString *actionSheetTitle = @"Selecione a opção desejada";
        NSString *cancelTitle = @"Cancelar";
        NSString *botao1 = @"Apagar pacientes";
        NSString *botao2 = @"Selecionar múltiplos pacientes";
        
        UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                      initWithTitle:actionSheetTitle
                                      delegate:self
                                      cancelButtonTitle:nil
                                      destructiveButtonTitle:cancelTitle
                                      otherButtonTitles:botao1, botao2, nil];
        
        [actionSheet setTag:1];
        [actionSheet showInView:self.view];
    }
}

- (IBAction)fechar:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        ;
    }];
}

@end
