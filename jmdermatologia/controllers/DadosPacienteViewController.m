//
//  DadosPacienteViewController.m
//  jmdermatologia
//
//  Created by Zeroum on 21/05/13.
//
//

#import "DadosPacienteViewController.h"
#import "Helper.h"

@interface DadosPacienteViewController ()

@end

@implementation DadosPacienteViewController
@synthesize nome;
@synthesize bairro;
@synthesize celular;
@synthesize cep;
@synthesize cidade;
@synthesize cpf;
@synthesize dataNascimento;
@synthesize email;
@synthesize endereco;
@synthesize formaContato;
@synthesize indicacao;
@synthesize paciente;
@synthesize rg;
@synthesize profissao;
@synthesize telefone;
@synthesize customCell;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    campos = [[NSMutableArray alloc] initWithCapacity:14];
    dadosPaciente = [[NSMutableArray alloc] initWithCapacity:14];
    
    [campos addObject:@"Nome"];
    if (paciente.nome != nil) {
        [dadosPaciente addObject:paciente.nome];
    } else {
        [dadosPaciente addObject:@""];
    }
    
    [campos addObject:@"Data de Nascimento:"];
    if (paciente.dataNascimento != nil) {
        [dadosPaciente addObject:paciente.dataNascimento];
    } else {
        [dadosPaciente addObject:@""];
    }
    
    [campos addObject:@"Profissão:"];
    if (paciente.profissao != nil) {
        [dadosPaciente addObject:paciente.profissao];
    } else {
        [dadosPaciente addObject:@""];
    }

    [campos addObject:@"CPF:"];
    if (paciente.cpf != nil) {
        [dadosPaciente addObject:paciente.cpf];
    } else {
        [dadosPaciente addObject:@""];
    }
    
    [campos addObject:@"RG:"];
    if (paciente.rg != nil) {
        [dadosPaciente addObject:paciente.rg];
    } else {
        [dadosPaciente addObject:@""];
    }
    
    [campos addObject:@"Endereço:"];
    if (paciente.endereco != nil) {
        [dadosPaciente addObject:paciente.endereco];
    } else {
        [dadosPaciente addObject:@""];
    }
    
    [campos addObject:@"Bairro:"];
    if (paciente.bairro != nil) {
        [dadosPaciente addObject:paciente.bairro];
    } else {
        [dadosPaciente addObject:@""];
    }
    
    [campos addObject:@"Cidade:"];
    if (paciente.cidade != nil) {
        [dadosPaciente addObject:paciente.cidade];
    } else {
        [dadosPaciente addObject:@""];
    }
        
    [campos addObject:@"CEP:"];
    if (paciente.cep != nil) {
        [dadosPaciente addObject:paciente.cep];
    } else {
        [dadosPaciente addObject:@""];
    }
    
    [campos addObject:@"Celular:"];
    if (paciente.celular != nil) {
        [dadosPaciente addObject:paciente.celular];
    } else {
        [dadosPaciente addObject:@""];
    }
    
    [campos addObject:@"Telefone:"];
    if (paciente.telefone != nil) {
        [dadosPaciente addObject:paciente.telefone];
    } else {
        [dadosPaciente addObject:@""];
    }
    
    [campos addObject:@"E-mail"];
    if (paciente.email != nil) {
        [dadosPaciente addObject:paciente.email];
    } else {
        [dadosPaciente addObject:@""];
    }
    
    [campos addObject:@"Indicação:"];
    if (paciente.indicacao) {
        [dadosPaciente addObject:paciente.indicacao];
    } else {
        [dadosPaciente addObject:@""];
    }
    
    
    formadecontato = [[NSMutableString alloc] initWithCapacity:20];
    
    NSInteger quantidadeFormaContato = 0;
    
    if (paciente.contatoSms == YES)
    {
        quantidadeFormaContato++;
        [formadecontato appendString:@"SMS"];
    }
    
    if (paciente.contatoEmail == YES)
    {
        if (quantidadeFormaContato > 0)
        {
            [formadecontato appendString:@" / E-mail"];
        }
        else
        {
            [formadecontato appendString:@"E-mail"];
        }
        quantidadeFormaContato++;
    }
    
    if (paciente.contatoTelefone == YES)
    {
        if (quantidadeFormaContato > 0)
        {
            [formadecontato appendString:@" / Telefone"];
        }
        else
        {
            [formadecontato appendString:@"Telefone"];
        }
    }
    
    [campos addObject:@"Forma de contato:"];
    [dadosPaciente addObject:formadecontato];
    
    [formadecontato release];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table View



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 14;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DadosPacienteViewCell"];
    
    if (cell == nil)
    {
        [[NSBundle mainBundle] loadNibNamed:@"DadosPacienteViewCell" owner:self options:nil];
        cell = customCell;
        self.customCell = nil;
    }
   
    UILabel *campo = (UILabel*)[cell viewWithTag:2];
    UILabel *valor = (UILabel*)[cell viewWithTag:3];
    
    campo.text = [campos objectAtIndex:indexPath.row];
    valor.minimumFontSize = 8;
    valor.adjustsFontSizeToFitWidth = YES;
    valor.text = [dadosPaciente objectAtIndex:indexPath.row];
    
    return cell;
}



#pragma mark - IBActions

- (IBAction)voltar:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)enviarEmail:(id)sender
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"template_email" ofType:@"html"];
    NSData *htmlData   = [NSData dataWithContentsOfFile:filePath];
    NSString *templateHTML = [[NSString alloc] initWithData:htmlData encoding:NSUTF8StringEncoding];
    
    NSString *body = [[NSString alloc] initWithFormat:templateHTML,
                      paciente.nome,
                      paciente.dataCadastro,
                      paciente.nome,
                      paciente.dataNascimento,
                      paciente.profissao,
                      paciente.cpf,
                      paciente.rg,
                      paciente.endereco,
                      paciente.bairro,
                      paciente.cidade,
                      paciente.cep,
                      paciente.celular,
                      paciente.telefone,
                      paciente.email,
                      paciente.indicacao,
                      formadecontato];
    
    MFMailComposeViewController* mailController = [[MFMailComposeViewController alloc] init];
    
    if ([MFMailComposeViewController canSendMail])
    {
        mailController.mailComposeDelegate = self;
        [mailController setToRecipients:[NSArray arrayWithObject:@"contato@jmdermatologia.com.br"]];
        [mailController setSubject:[NSString stringWithFormat:@"Dados paciente: %@",paciente.nome]];
        [mailController setMessageBody:body isHTML:YES];
        
        [self presentViewController:mailController animated:YES completion:^{
            ;
        }];
    }
    
    [mailController release];
    [body release];
    [templateHTML release];
}


- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error
{
	if (result == MFMailComposeResultSent)
    {
		NSLog(@"Email enviado!");
        [Helper alertViewMessage:@"Email Enviado com sucesso!" andTitle:@"Atenção"];
	}
	
    [self dismissViewControllerAnimated:YES completion:^{
        ;
    }];
}

@end
