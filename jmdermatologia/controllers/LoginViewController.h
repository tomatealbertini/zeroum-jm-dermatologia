//
//  LoginViewController.h
//  jmdermatologia
//
//  Created by Zeroum on 20/05/13.
//
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController <UIGestureRecognizerDelegate>


@property (nonatomic, assign) IBOutlet UITextField *senha;

- (IBAction)autenticar:(id)sender;
- (IBAction)fechar:(id)sender;

@end
