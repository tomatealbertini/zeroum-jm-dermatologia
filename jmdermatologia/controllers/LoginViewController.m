//
//  LoginViewController.m
//  jmdermatologia
//
//  Created by Zeroum on 20/05/13.
//
//

#import "LoginViewController.h"
#import "DadosViewController.h"

#import "Helper.h"
@interface LoginViewController ()

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.senha.secureTextEntry = YES;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    for (UIGestureRecognizer *gR in self.view.gestureRecognizers) {
        gR.delegate = self;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    //Touch gestures below top bar should not make the page turn.
    //EDITED Check for only Tap here instead.
    
    if ([gestureRecognizer isKindOfClass:[UITapGestureRecognizer class]]) {
        
        CGPoint touchPoint = [touch locationInView:self.view];
        
        NSLog(@"x: %f - y: %f",touchPoint.x,touchPoint.y);

        if (((touchPoint.x > 174 && touchPoint.x < 365) && (touchPoint.y > 486 && touchPoint.y < 515)) //bt favoritos esquerda
          ||((touchPoint.x > 485 && touchPoint.x < 535) && (touchPoint.y > 5 && touchPoint.y < 55))) //bt Voltar para indice
        {
            return NO;
        }
    }
    return YES;
}

- (BOOL)disablesAutomaticKeyboardDismissal
{
    return NO; //para funcionar o resign firts responder do senha, porque esta em um modal, ver a category criada e usada na viewcontroller.m
}

-(void)dismissKeyboard
{
    [self.senha resignFirstResponder];
}

- (IBAction)autenticar:(id)sender
{
    if ([self.senha.text isEqualToString:@"jm123"])
    {
        
        DadosViewController *dadosController = [[DadosViewController alloc] initWithNibName:@"DadosView"
                                                                                     bundle:nil];
        
        [self.navigationController pushViewController:dadosController
                                             animated:YES];
        [dadosController release];
    }
    else
    {
        [Helper alertViewMessage:@"Senha incorreta" andTitle:@"Atenção"];
    }
}

- (IBAction)fechar:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        ;
    }];
}

@end
