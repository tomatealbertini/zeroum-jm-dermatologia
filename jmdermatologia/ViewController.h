//
//  ViewController.h
//  jmdermatologia
//
//  Created by ZeroUm on 14/05/13.
//
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UIGestureRecognizerDelegate> {
    NSInteger proxTag;
}




@property (nonatomic, assign) IBOutlet UITextField *nome;
@property (nonatomic, assign) IBOutlet UITextField *dataNascimento;
@property (nonatomic, assign) IBOutlet UITextField *profissao;
@property (nonatomic, assign) IBOutlet UITextField *cpf;
@property (nonatomic, assign) IBOutlet UITextField *rg;
@property (nonatomic, assign) IBOutlet UITextField *endereco;
@property (nonatomic, assign) IBOutlet UITextField *cep;
@property (nonatomic, assign) IBOutlet UITextField *cidade;
@property (nonatomic, assign) IBOutlet UITextField *bairro;
@property (nonatomic, assign) IBOutlet UITextField *telefone;
@property (nonatomic, assign) IBOutlet UITextField *celular;
@property (nonatomic, assign) IBOutlet UITextField *email;
@property (nonatomic, assign) IBOutlet UITextField *indicacao;
@property (nonatomic, assign) IBOutlet UIButton *contatoSms;
@property (nonatomic, assign) IBOutlet UIButton *contatoEmail;
@property (nonatomic, assign) IBOutlet UIButton *contatoTelefone;
@property (nonatomic, assign) IBOutlet UIImageView *backGround;

- (IBAction)salvarPaciente:(id)sender;
- (IBAction)abrirInfo:(id)sender;

@end
