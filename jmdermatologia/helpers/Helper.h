//
//  Helper.h
//  guiademoteis
//
//  Created by Mac Mini 1 on 18/12/12.
//  Copyright (c) 2012 zeroum. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Helper : NSObject

+ (UIColor *)hexColor:(int)hex;
+ (void)alertViewMessage:(NSString *)message andTitle:(NSString *)title;

@end
