//
//  UINavigationController+KeyboardDismiss.m
//  jmdermatologia
//
//  Created by Zeroum on 23/05/13.
//
//

#import "UINavigationController+KeyboardDismiss.h"

@implementation UINavigationController(KeyboardDismiss)

- (BOOL)disablesAutomaticKeyboardDismissal
{
    return NO;
}

@end
