//
//  UINavigationController+KeyboardDismiss.h
//  jmdermatologia
//
//  Created by Zeroum on 23/05/13.
//
//

#import <Foundation/Foundation.h>

@interface UINavigationController (KeyboardDismiss)

- (BOOL)disablesAutomaticKeyboardDismissal;

@end