//
//  Helper.m
//  guiademoteis
//
//  Created by Mac Mini 1 on 18/12/12.
//  Copyright (c) 2012 zeroum. All rights reserved.
//

#import "Helper.h"


#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@implementation Helper

+ (UIColor *)hexColor:(int)hex {
	return UIColorFromRGB(hex);
}

+ (void)alertViewMessage:(NSString *)message andTitle:(NSString *)title {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    
    [alert show];
    [alert release];
}


@end
