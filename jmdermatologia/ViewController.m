//
//  ViewController.m
//  jmdermatologia
//
//  Created by ZeroUm on 14/05/13.
//
//

#import "ViewController.h"
#import "PacienteManager.h"
#import "LoginViewController.h"
#import "Helper.h"
#import "UINavigationController+KeyboardDismiss.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize nome;
@synthesize dataNascimento;
@synthesize profissao;
@synthesize cpf;
@synthesize rg;
@synthesize endereco;
@synthesize cep;
@synthesize cidade;
@synthesize bairro;
@synthesize telefone;
@synthesize celular;
@synthesize email;
@synthesize indicacao;
@synthesize contatoSms;
@synthesize contatoEmail;
@synthesize contatoTelefone;
@synthesize backGround;



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    for (UIGestureRecognizer *gR in self.view.gestureRecognizers) {
        gR.delegate = self;
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    [super dealloc];
}

#pragma mark - UITextField Delegate

-(BOOL)textFieldShouldReturn:(UITextField*)textField;
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder)
    {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    }
    else
    {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return NO; // We do not want UITextField to insert line-breaks.
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    UIImageView *imageBox = nil;

    if (textField.tag == 5 || textField.tag == 9 || textField.tag == 11)
    {
        imageBox = (UIImageView*)[self.view viewWithTag:textField.tag + 19];
    }
    else
    {
        imageBox = (UIImageView*)[self.view viewWithTag:textField.tag + 20];
    }
    
    [imageBox setImage:[UIImage imageNamed:@"hm-tap-dados"]];
    
    proxTag = textField.tag;
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    UIImageView *imageBox = nil;
    
    imageBox = (UIImageView*)[self.view viewWithTag:textField.tag + 20];
    
    BOOL flagRemoveTap = YES;
    
    if ((((textField.tag == 4) && (proxTag == 5)) || ((textField.tag == 5) && (proxTag == 4))) ||
        (((textField.tag == 8) && (proxTag == 9)) || ((textField.tag == 9) && (proxTag == 8))) ||
        (((textField.tag == 10)&& (proxTag == 11))|| ((textField.tag == 11)&& (proxTag == 10))))
    {
        flagRemoveTap = NO;
    }
    else if ((textField.tag == 5) || (textField.tag == 9) ||(textField.tag == 11))
    {
        imageBox = (UIImageView*)[self.view viewWithTag:textField.tag + 19];
    }
    
    if (flagRemoveTap == YES) {
        [imageBox setImage:[UIImage imageNamed:@"hm-dados"]];
    }    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	NSCharacterSet *nonNumberSet = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
	
	if (textField == self.dataNascimento)
    {
		if (range.length == 1)
        {
            return YES;
		}
        else
        {
            if ([textField.text length] == 2 || [textField.text length] == 5)
            {
                textField.text = [NSString stringWithFormat:@"%@/", textField.text];
                
            }
            else if ([textField.text length] == 10)
            {
                return NO;
            }
            
			return ([string stringByTrimmingCharactersInSet:nonNumberSet].length > 0);
		}
	} else if (textField == self.telefone) {
        NSString* totalString = [NSString stringWithFormat:@"%@%@",textField.text,string];
        
        // if it's the phone number textfield format it.
            if (range.length == 1) {
                // Delete button was hit.. so tell the method to delete the last char.
                textField.text = [self formatPhoneNumber:totalString deleteLastChar:YES nonoDigito:NO];
            } else {
                textField.text = [self formatPhoneNumber:totalString deleteLastChar:NO nonoDigito:NO];
            }
            return false;
    } else if (textField == self.celular) {
        NSString* totalString = [NSString stringWithFormat:@"%@%@",textField.text,string];
        
        // if it's the phone number textfield format it.
        if (range.length == 1) {
            // Delete button was hit.. so tell the method to delete the last char.
            textField.text = [self formatPhoneNumber:totalString deleteLastChar:YES nonoDigito:YES];
        } else {
            textField.text = [self formatPhoneNumber:totalString deleteLastChar:NO nonoDigito:YES];
        }
        return false;
    } else if (textField == self.cep) {
        if (range.length >= 4)
        {
            return YES;
		}
        else
        {
            if ([textField.text length] == 5 )
            {
                textField.text = [NSString stringWithFormat:@"%@-", textField.text];
                
            }
            else if ([textField.text length] == 9)
            {
                return NO;
            }
            
			return ([string stringByTrimmingCharactersInSet:nonNumberSet].length > 0);
		}
    } else if (textField == self.cpf) {
        if (range.length >= 2)
        {
            return YES;
		}
        else
        {
            if ([textField.text length] == 3 || [textField.text length] == 7 )
            {
                textField.text = [NSString stringWithFormat:@"%@.", textField.text];
                
            }
            else if ([textField.text length] == 14)
            {
                return NO;
            } else if ([textField.text length] == 11) {
                textField.text = [NSString stringWithFormat:@"%@-", textField.text];
            }
            
			return ([string stringByTrimmingCharactersInSet:nonNumberSet].length > 0);
		}
    }
    else if (textField == self.nome)
    {
        if ([textField.text length] == 0) {
            textField.text = [NSString stringWithFormat:@"                          %@",textField.text];
        }
    }
    
    return YES;
}

- (NSString*)formatPhoneNumber:(NSString*)simpleNumber deleteLastChar:(BOOL)deleteLastChar nonoDigito:(BOOL)nonoDigito {
    
    if(simpleNumber.length == 0)
        return @"";
    
    // use regex to remove non-digits(including spaces) so we are left with just the numbers
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[\\s-\\(\\)]" options:NSRegularExpressionCaseInsensitive error:&error];
    simpleNumber = [regex stringByReplacingMatchesInString:simpleNumber options:0 range:NSMakeRange(0, [simpleNumber length]) withTemplate:@""];
    
    // check if the number is to long
    NSInteger qtdeDigitos = 10;
    if (nonoDigito == YES)
    {
        qtdeDigitos = 11;
    }
    if(simpleNumber.length > qtdeDigitos)
    {
        // remove last extra chars.
        simpleNumber = [simpleNumber substringToIndex:qtdeDigitos];
    }
    
    if(deleteLastChar)
    {
        // should we delete the last digit?
        simpleNumber = [simpleNumber substringToIndex:[simpleNumber length] - 1];
    }
    
    // format the number.. if it's less then 7 digits.. then use this regex.
    if(simpleNumber.length < 7)
        simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{2})(\\d+)"
                                                               withString:@"($1) $2"
                                                                  options:NSRegularExpressionSearch
                                                                    range:NSMakeRange(0, [simpleNumber length])];
    else   // else do this one..
        if (nonoDigito == YES)
        {
            if (simpleNumber.length < qtdeDigitos)
            {
                simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{2})(\\d{4})(\\d+)"
                                                                       withString:@"($1) $2-$3"
                                                                          options:NSRegularExpressionSearch
                                                                            range:NSMakeRange(0, [simpleNumber length])];
            }
            else
            {
                simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{2})(\\d{5})(\\d+)"
                                                                       withString:@"($1) $2-$3"
                                                                          options:NSRegularExpressionSearch
                                                                            range:NSMakeRange(0, [simpleNumber length])];
            }
        }
        else
        {
            simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{2})(\\d{4})(\\d+)"
                                                                   withString:@"($1) $2-$3"
                                                                      options:NSRegularExpressionSearch
                                                                        range:NSMakeRange(0, [simpleNumber length])];
        }
    return simpleNumber;
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    //Touch gestures below top bar should not make the page turn.
    //EDITED Check for only Tap here instead.
    
    if ([gestureRecognizer isKindOfClass:[UITapGestureRecognizer class]]) {
        
        CGPoint touchPoint = [touch locationInView:self.view];
        
        NSLog(@"x: %f - y: %f",touchPoint.x,touchPoint.y);
        
        if (((touchPoint.x > 675 && touchPoint.x < 723) && (touchPoint.y > 27 && touchPoint.y < 75)) //bt info
            ||((touchPoint.x > 234 && touchPoint.x < 367) && (touchPoint.y > 930 && touchPoint.y < 985)) //limpar
            ||((touchPoint.x > 404 && touchPoint.x < 537) && (touchPoint.y > 930 && touchPoint.y < 985)) //concluir
            ||((touchPoint.x > 89 && touchPoint.x < 112) && (touchPoint.y > 809 && touchPoint.y < 832)) //email
            ||((touchPoint.x > 238 && touchPoint.x < 261) && (touchPoint.y > 809 && touchPoint.y < 832)) //sms
            ||((touchPoint.x > 366 && touchPoint.x < 389) && (touchPoint.y > 809 && touchPoint.y < 832))) //telefone
        {
            [self removerTapGesture];
            return NO;
        }
    }
    return YES;
}


- (void)removerTapGesture {
    
    NSLog(@"removerTapGesture");
    
    UIGestureRecognizer* tapRecognizer = nil;
    for (UIGestureRecognizer* recognizer in self.view.gestureRecognizers) {
        if ([recognizer isKindOfClass:[UITapGestureRecognizer class]]) {
            tapRecognizer = recognizer;
            break;
        }
    }
    
    if (tapRecognizer) {
        [self.view removeGestureRecognizer:tapRecognizer];
        [self.view removeGestureRecognizer:tapRecognizer];
    }
}

#pragma mark - Alert View
- (void)exibirMensagemEncerramento {
    
    UIView *viewEncerramento = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 768, 1024)];
    UIImageView *imagem = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 768, 1024)];
    
    [imagem setImage:[UIImage imageNamed:@"bg-encerramento"]];
    [viewEncerramento addSubview:imagem];
    
    [viewEncerramento setAlpha:0.0f];
    
    [self.view addSubview:viewEncerramento];
    
    [UIView animateWithDuration:0.5f animations:^{
        [viewEncerramento setAlpha:1.0f];
    }];
    
    [imagem release];
}

- (void)sairAplicativo {
    exit(0);
}

#pragma mark - Private

- (BOOL)verificarCamposObrigatorios
{
    if ([self.nome.text isEqualToString:@""])
    {
        [Helper alertViewMessage:@"Campo Nome obrigatório" andTitle:@"Atenção"];
        return NO;
    }
    else if ([self.cpf.text isEqualToString:@""])
    {
        [Helper alertViewMessage:@"Campo CPF obrigatório" andTitle:@"Atenção"];
        return NO;
    }
    else if ([self.rg.text isEqualToString:@""])
    {
        [Helper alertViewMessage:@"Campo RG obrigatório" andTitle:@"Atenção"];
        return NO;
    }
    else if (([self.telefone.text isEqualToString:@""]) && ([self.celular.text isEqualToString:@""]))
    {
        [Helper alertViewMessage:@"Campo Telefone/Celular obrigatório" andTitle:@"Atenção"];
        return NO;
    }
    return YES;
}

#pragma mark - IBActions

- (IBAction)salvarPaciente:(id)sender {
    
    if ([self verificarCamposObrigatorios] == YES) {
        Paciente *paciente = [[Paciente alloc] init];
        
        NSString *nomePac = [NSString stringWithString:[nome.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
        [paciente setNome:[nomePac capitalizedString]];
        [paciente setDataNascimento:dataNascimento.text];
        [paciente setProfissao:profissao.text];
        [paciente setCpf:cpf.text];
        [paciente setRg:rg.text];
        [paciente setEndereco:endereco.text];
        [paciente setCep:cep.text];
        [paciente setCidade:cidade.text];
        [paciente setBairro:bairro.text];
        [paciente setTelefone:telefone.text];
        [paciente setCelular:celular.text];
        [paciente setEmail:email.text];
        [paciente setIndicacao:indicacao.text];
        [paciente setContatoEmail:contatoEmail.selected];
        [paciente setContatoSms:contatoSms.selected];
        [paciente setContatoTelefone:contatoTelefone.selected];
        
        [[PacienteManager sharedManager] gravarPaciente:paciente];
        
        [self exibirMensagemEncerramento];
        
        [self performSelector:@selector(sairAplicativo) withObject:nil afterDelay:6.0];
        [paciente release];
    }
}

-(void)dismissKeyboard {
    
    NSLog(@"dismissKeyboard");
    
    [self.nome resignFirstResponder];
    [self.dataNascimento resignFirstResponder];
    [self.profissao resignFirstResponder];
    [self.cpf resignFirstResponder];
    [self.rg resignFirstResponder];
    [self.endereco resignFirstResponder];
    [self.cep resignFirstResponder];
    [self.cidade resignFirstResponder];
    [self.bairro resignFirstResponder];
    [self.telefone resignFirstResponder];
    [self.celular resignFirstResponder];
    [self.email resignFirstResponder];
    [self.indicacao resignFirstResponder];
}

- (IBAction)limparFormulario:(id)sender
{
    for (UITextField *b in self.view.subviews)
    {
        if ([b isKindOfClass:[UITextField class]])
        {
            [b setText:@""];
        }
        else if ([b isKindOfClass:[UIButton class]])
        {
            [b setSelected:NO];
        }
    }
}

- (IBAction)habilitarTipoEnvioContato:(id)sender
{
    UIButton *bt = (UIButton*)sender;
    bt.selected = !bt.selected;
}

- (IBAction)abrirInfo:(id)sender
{
    LoginViewController *loginController = [[LoginViewController alloc] initWithNibName:@"LoginView" bundle:nil];
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:loginController];
    
    [navController setModalPresentationStyle:UIModalPresentationFormSheet];
    [navController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [navController setNavigationBarHidden:YES];
    
    [self presentViewController:navController animated:YES completion:^{
        ;
    }];
    [loginController release];
}

@end
